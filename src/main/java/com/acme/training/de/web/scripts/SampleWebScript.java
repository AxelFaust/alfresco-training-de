package com.acme.training.de.web.scripts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.site.SiteInfo;
import org.alfresco.service.cmr.site.SiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.dynamicextensionsalfresco.annotations.AlfrescoService;
import com.github.dynamicextensionsalfresco.annotations.ServiceType;
import com.github.dynamicextensionsalfresco.webscripts.annotations.Authentication;
import com.github.dynamicextensionsalfresco.webscripts.annotations.FormatStyle;
import com.github.dynamicextensionsalfresco.webscripts.annotations.Transaction;
import com.github.dynamicextensionsalfresco.webscripts.annotations.Uri;
import com.github.dynamicextensionsalfresco.webscripts.annotations.WebScript;

/**
 * @author Axel Faust, <a href="http://acosix.de">Acosix GmbH</a>
 */
@Component
@WebScript(baseUri = "acme/de")
public class SampleWebScript
{

    @Autowired(required = true)
    @AlfrescoService(ServiceType.DEFAULT)
    protected SiteService siteService;

    @Transaction(bufferSize = 2048)
    @Authentication
    @Uri(value = "/my-sites", formatStyle = FormatStyle.ANY, defaultFormat = "json")
    public Map<String, Object> displayMySites()
    {
        final Map<String, Object> model = new HashMap<>();

        final List<SiteInfo> sites = this.siteService.listSites(AuthenticationUtil.getRunAsUser());
        model.put("sites", sites);

        return model;
    }
}
