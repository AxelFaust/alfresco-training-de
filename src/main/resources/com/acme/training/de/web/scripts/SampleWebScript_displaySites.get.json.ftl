<#compress><#escape x as jsonUtils.encodeJSONString(x)>
{
    "sites": [
        <#list sites as site>
        {
            "shortName" : "${site.shortName}",
            "title" : "${site.title}"
        }<#if site_has_next>,</#if>
        </#list>
    ]
}
</#escape></#compress>